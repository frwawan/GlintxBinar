const data = require('./lib/arrayFactory.js');
const test = require('./lib/test.js');

console.log(data)

/*
 * Code Here!
 * */

// Optional
function clean(data) {
  return data.filter(i => typeof i === 'number');
}
// Should return array
function sortAscending(data) {
  data= clean(data)
  // Code Here
  data =clean(data)
  for (let i=0; i<data.length; i++){
    for (let j = 0; j< data.length-i;j++){
      if(data[j]>data[j+1]){
        let tmp =data[j];
        data[j]=data[j+1];
        data[j+1]=tmp;
      }
    }
  }
  return data;
}

// Should return array
function sortDescending(data) {
  data= clean(data)
  // Code Here
  data =clean(data)
  for (let i=0; i<data.length; i++){
    for (let j = 0; j< data.length-i;j++){
      if(data[j]<data[j+1]){
        let tmp =data[j];
        data[j]=data[j+1];
        data[j+1]=tmp;
      }
    }
  }
  return data;
}

// DON'T CHANGE
test(sortAscending, sortDescending, data);

