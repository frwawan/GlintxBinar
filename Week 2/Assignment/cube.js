const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

function volCube(radius){
    return radius**3;
}

function inputR(){
    rl.question(`Radius:`, radius =>{
        if(!isNaN(radius)){
            console.log(`\nVolume cube is ${volCube(radius)}`)
            rl.close()
        }else{
            console.log(`Radius must be number`)
            input()
        }
    })
}


input()

