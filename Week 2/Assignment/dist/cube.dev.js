"use strict";

var readline = require('readline');

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

function volCube(radius) {
  return Math.pow(radius, 3);
}

function inputR() {
  rl.question("Radius:", function (radius) {
    if (!isNaN(radius)) {
      console.log("\nVolume cube is ".concat(volCube(radius)));
      rl.close();
    } else {
      console.log("Radius must be number");
      input();
    }
  });
}

input();