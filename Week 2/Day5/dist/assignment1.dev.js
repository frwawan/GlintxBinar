"use strict";

/*
 * DON'T CHANGE
 * */
var data = [];
var randomNumber = Math.floor(Math.random() * 100);

function createArray() {
  for (var i = 0; i < randomNumber; i++) {
    data.push(createArrayElement());
  } // Recursive


  if (data.length == 0) {
    createArray();
  }
}

function createArrayElement() {
  var random = Math.floor(Math.random() * 1000);
  return [null, random][Math.floor(Math.random() * 2)];
}

createArray();
console.log(data);

function clean(data) {
  // Code here
  return data.filter(function (i) {
    return i !== null;
  });
}
/*
 * DON'T CHANGE
 * */


if (process.argv.slice(2)[0] == "test") {
  try {
    clean(data).forEach(function (i) {
      if (i == null) {
        throw new Error("Array still contains null");
      }
    });
  } catch (err) {
    console.error(err.message);
  }
}

console.log(clean(data));