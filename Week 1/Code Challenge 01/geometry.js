const readline = require('readline')
const rl =readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

//choose what do you want to calculate
//rectangular solid or sphere



// Calculate Volume of a Rectangular Solid
function calculateVolumeRectangularSolid(length,width,height){
    return length*width*height
}

function inputLength(){
    rl.question(`Length:`, length =>{
        if(!isNaN(length)){
            inputWidth(length)
        }else {
            console.log(`length must be a number\n`)
            inputLength()
        }
    })
}
function inputWidth(length){
    rl.question(`Width:`, width =>{
        if (!isNaN(width)){
            inputHeight(length,width)
    
        }else{
            console.log(`length must be a number\n`)
            inputWidth()
        }
    })
}

function inputHeight(length,width){
    rl.question(`Height:`, height =>{
        if (!isNaN(height)){
            console.log(`\nvolume ${calculateVolumeRectangularSolid(length, width, height)}`)
            rl.close()
        }else{
            console.log(`height must be a number\n`)
            inputHeight(length,width)
        }
    })
}

console.log(`Volume of Rectangular Solid`)
console.log(`===========================`);
inputLength()


// // Volume of a sphere
const pi = 3.14;

function CalculateVolumeSphere(r){
    let SphereBase = 4/3 * pi;
    return SphereBase * r;
}
// console.log(
//     CalculateVolumeSphere(4)
// )

function inputradius(){
    rl.question(`radius:`, radius =>{
        if (!isNaN(`radius`)){
        console.log(`Volume ${CalculateVolumeSphere(radius)}`)
        rl.close()
        }else{
            console.log(`radius must be a number\n`)
            inputradius()
        }
    })
}


