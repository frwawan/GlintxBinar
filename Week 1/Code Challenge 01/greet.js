// GlintXBinar Assignment

const readline = require('readline');
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
})

// Const recent year
const currentDate = new Date();
const currentYear = currentDate.getFullYear();
// write function
function greet(name, address, birthday) {
// state variable for age
var birthday = currentYear - birthday;
// print output
console.log("Hello, " + name + "!" + " looks like you're " + birthday + " years old," + " And you lived in " + address + "!");

}
console.log("GlintXBinar Assignment\n")
// GET User's Name
rl.question("What is your name? ",  name => { 
  // GET User's Address
  rl.question("Which city do you live? ", address => {
    // GET User's Birthday
    rl.question("When was your birthday year? ", birthday => {
      greet(name, address, birthday)

      rl.close()
    })
  })
})

rl.on("close", () => {
  process.exit()
})
