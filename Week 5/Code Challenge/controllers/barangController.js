const { barang, pemasok } = require('../models')

class BarangController {

    async getAll(req, res) {
        barang.find({}).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        barang.findOne({
            _id: req.params.id
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const Pemasok = await pemasok.findOne({
            _id: req.body.id_pemasok
        })

        barang.create({
            nama: req.body.nama,
            harga: eval(req.body.harga),
            pemasok: Pemasok,
            image: req.file === undefined ? "" : req.file.filename
        }).then(result => {
            res.json({
                status: "succes",
                data: result
            })
        })
    }

    async update(req, res) {
        const Pemasok = await pemasok.findOne({
            _id: req.body.id_pemasok
        })

        barang.findOneAndUpdate({
            _id: req.params.id
        }, {
            nama: req.body.nama,
            harga: eval(req.body.harga),
            pemasok: Pemasok,
            image: req.file === undefined ? "" : req.file.filename
        }).then(result => {
            res.json({
                status: "update succes",
                data: result
            })
        })
    }

    async delete(req, res) {
        barang.delete({
            _id: req.params.id
        }).then(() => {
            res.json({
                status: "Data Barang Tidak Ada",
                data: null
            })
        })
    }
}

module.exports = new BarangController