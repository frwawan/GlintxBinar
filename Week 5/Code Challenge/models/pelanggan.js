const mongoose = require('mongoose');
const mongoose_delete = require('mongoose-delete');

const pelangganSchema = new mongoose.Schema({
    nama:{
        type: String,
        required: true
    }    
},{
    timestamps:{
        createdAt:'created_at',
        updatedAt:'updated_at'
    },
    versionKey:false
})

pelangganSchema.plugin(mongoose_delete,{overrideMethod:'all'});
module.exports = pelanggan = mongoose.model('pelanggan',pelangganSchema,'pelanggan')