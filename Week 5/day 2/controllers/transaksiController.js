const client = require('../models/connection.js');
const { ObjectID } = require('mongodb')

class TransaksiController{
    async getAll(req, res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.find({}).toArray().then(result =>{
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        transaksi.findOne({ 
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }


    async create(req, res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        const barang = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })

        let total = eval(barang.harga.toString()) = req.body.jumlah

        transaksi.insertOne({
            barang: barang,
            pelanggan : pelanggan,
            jumlah: req.body.jumlah,
            total: total
        }).then(result =>{
            res.json({
                status: "success",
                data: result.ops[0]
            })
        })
    }

    async update(req, res){
        const penjualan = client.db('penjualan')
        const transaksi = penjualan.collection('transaksi')

        const barang = await penjualan.collection('barang').findOne({
            _id: new ObjectId(req.body.id_barang)
        })

        const pelanggan = await penjualan.collection('pelanggan').findOne({
            _id: new ObjectId(req.body.id_pelanggan)
        })

        let total = eval(barang.harga.toString()) * req.body.jumlah

        transaksi.updateOne({
            _id: new ObjectId(req.params.id)
          }, {
            $set: {
              barang: barang,
              pelanggan: pelanggan,
              jumlah: req.body.jumlah,
              total: total
            }
          }).then(() => {
            return transaksi.findOne({
              _id: new ObjectId(req.params.id)
            })
          }).then(result => {
            res.json({
              status: 'success',
              data: result
            })
          })
        }

        async delete(req, res) {
            const penjualan = client.db('penjualan') // Connect to penjualan database
            const transaksi = penjualan.collection('transaksi') // Connect to transaksi collection / table
        
            // delete data depends on req.params.id
            transaksi.deleteOne({
              _id: new ObjectId(req.params.id)
            }).then(result => {
              res.json({
                status: 'success',
                data: null
              })
            })
        }
    }


    module.exports = new TransaksiController;
