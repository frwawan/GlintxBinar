const client = require('../models/connection.js');
const { ObjectID } = require('mongodb')

class PelangganController{
    async getAll(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.find({}).toArray().then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async getOne(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.findOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "success",
                data: result
            })
        })
    }

    async create(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.insertOne({
            nama: req.body.nama,
        }).then(result => {
            res.json({
                status: "succes",
                data: result.ops[0]
            })
        })
    }

    async update(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                nama: req.body.nama
            }
        }).then(() => {
            return pelanggan.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "succes",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const pelanggan = penjualan.collection('pelanggan')

        pelanggan.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "succes",
            })
        })
    }
}

module.exports = new PelangganController;