const client = require('../models/connection.js');
const { ObjectID } = require('mongodb')

class BarangController{
    async getAll(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')

        barang.find({}).toArray().then(result =>{
            res.json({
                status : "succes",
                data : result
            })
        })
    }

    async getOne(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')

        barang.findOne(req, res)({
            _id: new ObjectID(req.params.id)
        }).then(result=>{
            res.json({
                status : "succes",
                data : result
            })
        })
    }

    async create(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')
        const pemasok = await penjualan.collection('pemasok').findOnes({
            _id: new ObjectID(req.body.id_pemasok)
        })

        barang.insertOne({
            nama:req.body.nama,
            harga:req.body.harga,
            pemasok : pemasok
        }).then(result =>{
            res.json({
                status: "succes",
                data: result
            })
        })
    }

    async update(req, res){
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')
        const pemasok = await penjualan.collection('pemasok').findOnes({
            _id: new ObjectID(req.body.id_pemasok)
        })

        barang.updateOne({
            _id: new ObjectId(req.params.id)
        }, {
            $set: {
                nama: req.body.nama,
                harga: req.body.harga,
                pemasok: pemasok
            }
        }).then(() => {
            return barang.findOne({
                _id: new ObjectId(req.params.id)
            })
        }).then(result => {
            res.json({
                status: "succes",
                data: result
            })
        })
    }

    async delete(req, res) {
        const penjualan = client.db('penjualan')
        const barang = penjualan.collection('barang')
    
        barang.deleteOne({
            _id: new ObjectId(req.params.id)
        }).then(result => {
            res.json({
                status: "succes",
            })
        })
    }
}

module.exports = new BarangController;