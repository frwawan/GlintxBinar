const express = require ('express')
const app = express();
const bodyParser = require('body-parser')
const transaksiRoutes = require('./routes/transaksiRoutes.js')
const pelangganRoutes = require('./routes/pelangganRoutes.js')
const barangRoutes = require('./routes/barangRoutes.js')
const pemasokRoutes = require('./routes/pemasokRoutes.js')


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use('/transaksi', transaksiRoutes)
app.use('/pelanggan',pelangganRoutes)
app.use('/pemasok',pemasokRoutes)
app.use('/barang',barangRoutes)

app.listen(3000);