const express = require('express') 
const router = express.Router() 
const TransaksiController = require('../controllers/transaksiController.js') 
const transaksiValidator = require('../middlewares/validators/transaksiValidator.js') 

router.get('/', TransaksiController.getAll) 
router.get('/:id', TransaksiController.getOne)
router.post('/create', transaksiValidator.create, TransaksiController.create) 
router.put('/update/:id', transaksiValidator.update, TransaksiController.update) 
router.delete('/delete/:id', TransaksiController.delete) 

module.exports = router; 
