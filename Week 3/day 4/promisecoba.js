//import fs
//fs unutk ngeread dan ngewrite file
const fs = require ('fs');

//start making promise object
const readFile = (file,options) => new Promise(fullfill, reject) => {
    fs.readFile(file, options, (err, content) => {
        if(err){
            reject(err)
        }
        return fullfill(content)
    })
}
//end of making promise object

//start use promise
readFile('contents/content.txt', 'utf-8')
    .then(content){
        console.log('succes')
    }
    .catch