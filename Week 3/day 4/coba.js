// Normal function
function totalBayarNormal(diskon,hargaAsli){
    return hargaAsli - (diskon/100 * hargaAsli)
}

let harga1 = totalBayarNormal(10,100000)
let harga2 = totalBayarNormal(10,200000)
let harga3 = totalBayarNormal(10,300000)
console.log(harga1);
console.log(harga2)
console.log(harga3);

// arrow function
let totalBayarArrow = (diskon, hargaAsli) => {
    return hargaAsli = (diskon/100 * hargaAsli)
}

let diskonArrow = 10

let harga4 = totalBayarArrow(diskonArrow, 400000)
let harga5 = totalBayarArrow(diskonArrow, 500000)
let harga6 = totalBayarArrow(diskonArrow, 600000)

console.log(harga4);
console.log(harga5);
console.log(harga6);

//currying
let totalBayarCurrying = diskon => hargaAsli => {
    return hargaAsli = (diskon/100 * hargaAsli)
}

console.log(totalBayarCurrying(25)(2));

let diskonCurrying = totalBayarCurrying(10);

let harga7 =diskonCurrying(700000)
let harga8 =diskonCurrying(800000)
let harga9 =diskonCurrying(900000)

console.log(harga7)
console.log(harga8)
console.log(harga9)