const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err)
      return resolve(content)
    })
  })

  const writeFile = (file,content) => new Promise ((resolve, reject) => {
      fs.writeFile(file,content, err =>{
          if (err) return reject(err)
          return resolve()
      })
  })

  const
    read = readFile('utf-8')

    async function mergedContent (){
    try {

    const result = await Promise.all([
        read('contents/content1.txt'),
        read('contents/content2.txt'),
        read('contents/content3.txt'),
        read('contents/content4.txt'),
        read('contents/content5.txt'),
        read('contents/content6.txt'),
        read('contents/content7.txt'),
        read('contents/content8.txt'),
        read('contents/content9.txt'),
        read('contents/content10.txt')
    ])

    await writeFile('contents/result.txt', result.join(''))
  } catch (e) {
    throw e
  }
    return read('contents/result.txt')
}

mergedContent()
  .then(result => {
    console.log(result) // content of result.txt
  }).catch(err => {
    console.log('Error to read/write file, error: ', err)
  }).finally(() => {
    console.log('Terbaique!!');
  })