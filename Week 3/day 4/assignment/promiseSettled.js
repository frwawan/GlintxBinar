const fs = require('fs')

const readFile = options => file => new Promise((resolve, reject) => {
    fs.readFile(file, options, (err, content) => {
      if (err) return reject(err)
      return resolve(content)
    })
  })

  const writeFile = (file, content) => new Promise((resolve, reject) => {
    fs.writeFile(file, content, err => {
      if (err) return reject(err)
      return resolve()
    })
  })

  const 
    read = readFile('utf-8')
    files = ['contents/content1.txt','contents/content2.txt','contents/content3.txt','contents/content4.txt','contents/content5.txt','contents/content6.txt','contents/content7.txt','contents/content8.txt','contents/content9.txt','contents/content10.txt']

    Promise.allSettled(files.map(file => read('${file}')))
    .then(results => {
        console.log(results)
    })