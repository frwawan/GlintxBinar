/* Make BangunDatar Abstract Class */
class BangunDatar {

    // Make constructor with name variable/property
    constructor(name) {
      if (this.constructor === BangunDatar) {
        throw new Error('This is abstract!')
      }
        this.name = name
    }
  
    // menghitungLuas instance method
    menghitungLuas() {
      console.log('Luas bangun datar');
    }
  
    // menghitungKeliling instance method
    menghitungKeliling() {
      console.log('Keliling bangun datar');
    }
  }
  /* End BangunDatar Abstract Class */
  
  module.exports = BangunDatar
  