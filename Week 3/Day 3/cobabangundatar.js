const persegi = require('./persegi.js')
const persegipanjang = require('./persegipanjang.js')
const BangunDatar = require('./bangundatar.js')

const cobapersegi = new persegi(10)
console.log(cobapersegi.menghitungLuas())

const cobapersegipanjang = new persegipanjang(10, 20)
console.log(cobapersegipanjang.menghitungLuas());

const bd = new BangunDatar('asdasd')
bd.menghitungLuas()
