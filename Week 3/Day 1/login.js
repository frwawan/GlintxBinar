const EventEmitter = require('events');
const readline = require('readline');
const corona = require('./corona.js');

const my = new EventEmitter()
const rl = readline.createInterface({
    input: process.stdin,
    output:process.stdout
})


// Registering a listener
my.on("Login Failed", function(email) {
    // TODO: Saving the login trial count in the database
    console.log(email, "is failed to login!");
  })
  
const user = {
    login(email, password) {
      const passwordStoredInDatabase = "123456";
  
      if (password !== passwordStoredInDatabase) {
        my.emit("Login Failed", email); // Pass the email to the listener
      } else {
        // Do something
        corona.inputOption();
     
      }
    }
  }
  
  rl.question("Email: ", function(email) {
    rl.question("Password: ", function(password) {
      user.login(email, password) // Run login function
    })
  })
  
rl.on("close", () =>{
    process.exit()
})  

module.exports.rl = rl