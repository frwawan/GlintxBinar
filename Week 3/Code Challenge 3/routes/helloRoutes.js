const express = require('express')
const router = express.Router()
const HelloController = require('../controllers/helloController.js')

router.get('/', HelloController.hello)

module.exports = router;