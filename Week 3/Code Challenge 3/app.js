const express = require('express')
const app = express()
const helloRoutes = require('./routes/helloRoutes.js')
const indexRoutes = require ('./routes/indexRoutes.js')
const wawanRoutes = require ('./routes/wawanRoutes.js')

app.use(express.static('public'))

app.use('/', indexRoutes)

app.use('/hello', helloRoutes)

app.use('/wawan', wawanRoutes)



app.listen(8080)