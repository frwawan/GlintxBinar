const express = require('express')
const router = express.Router()
const WawanController = require('../controllers/wawanController.js')

router.get('/', WawanController.name)

module.exports = router;
