const express = require('express')
const app = express()
const helloRoutes = require('./routes/helloRoutes.js')
const indexRoutes = require ('./routes/indexRoutes.js')
const wawanRoutes = require ('./routes/wawanRoutes.js')

app.use(express.static('public'))

app.use('/', indexRoutes)

app.use('/hello', helloRoutes)

app.use('/wawan', wawanRoutes)



app.listen(8080)
// app.get('/', (req, res) => {
//     console.log("blubla")
//     res.render('top.ejs')
// })
// app.get('/hello', (req, res)=>{
//     console.log("You are accesing Hellow World HTML")
//     res.render('hello.ejs')
// })
// app.get('/wawan',(req,res)=>{
//     console.log("You are accessing tugas woy");
//     res.render('wawan.ejs')
// })
// app.get('/index',(req,res)=>{
//     console.log("You are accesing index");
//     res.render('index.ejs')
// })
