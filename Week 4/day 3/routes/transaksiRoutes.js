const express = require('express')
const router = express.Router();
const TransaksiController = require('../controllers/transaksiController.js')

router.get('/', TransaksiController.getAll)
router.get('/:id', TransaksiController.getOne)

module.exports = router;