const connection = require('../models/connection.js') // import connection

class TransaksiController {

  // Function getAll transaksi table
  async getAll(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id ORDER by t.id" // make an query varible

      // Run query
      connection.query(sql, function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

  // Function getOne transaksi table
  async getOne(req, res) {
    try {
      var sql = "SELECT t.id as id_transaksi, b.nama as barang, p.nama as pelanggan, t.waktu, t.jumlah, t.total FROM transaksi t JOIN barang b ON t.id_barang = b.id JOIN pelanggan p ON t.id_pelanggan = p.id WHERE t.id = ?" // make an query varible

      // Run query
      connection.query(sql, [req.params.id], function(err, result) {
        if (err) throw err; // If error

        // If success it will return JSON of result
        res.json({
          status: "success",
          data: result[0]
        })
      });
    } catch (e) {
      // If error will be send Error JSON
      res.json({
        status: "Error"
      })
    }
  }

}

module.exports = new TransaksiController;
