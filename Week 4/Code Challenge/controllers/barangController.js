const {
    Barang,
    Pemasok
  } = require("../models")
  const {
    check,
    validationResult,
    matchedData,
    sanitize
  } = require('express-validator'); //form validation & sanitize form params
  
  class BarangController {
  
    constructor() {
      Pemasok.hasMany(Barang, {
        foreignKey: 'id_pemasok'
      })
      Barang.belongsTo(Pemasok, {
        foreignKey: 'id_pemasok'
      })
    }
    // Get All data from barang
    async getAll(req, res) {
      Barang.findAll({ // find all data of Barang table
        attributes: ['id', 'nama', 'harga', ['createdAt', 'waktu'], 'image'], // just these attributes that showed
        include: [{
          model: Pemasok,
          attributes: ['nama'] // just this attrubute from pemasok that showed
        }]
      }).then(barang => {
        res.json(barang) // Send response JSON and get all of Barang table
      })
    }
    // Get One data from Barang
    async getOne(req, res) {
      Barang.findOne({ // find one data of Barang table
        where: {
          id: req.params.id // where id of Barang table is equal to req.params.id
        },
        attributes: ['id', 'nama', 'harga', ['createdAt', 'waktu'], 'image'], // just these attributes that showed
        include: [{
          model: Pemasok,
          attributes: ['nama'] // just this attrubute from Pemasok that showed
        }]
      }).then(barang => {
        res.json(barang) // Send response JSON and get one of Barang table depend on req.params.id
      })
    }
    // Create Barang data
    async create(req, res) {
      Pemasok.findOne({ // find one data of Barang table
        where: {
          id: req.body.id_pemasok // where id of Pemasok table is equal to req.body.id_pemasok
        },
        attributes: ['nama'] // Get nama from Pemasok
      })
        // Barang table create data
        return Barang.create({
          nama : req.body.nama,
          harga: req.body.harga,
          id_pemasok: req.body.id_pemasok,
          image: req.file===undefined?"":req.file.filename,
      }).then(newbarang => {
        // Send response JSON and get one of Barang table that we've created
        res.json({
          "status": "success",
          "message": "barang added",
          "data": newbarang
        })
      })
    }
    // Update Barang data
    async update(req, res) {
      Pemasok.findOne({ // find one data of Pemasok table
        where: {
          id: req.body.id_pemasok // where id of Pemasok table is equal to req.body.id_pemasok
        },
        attributes: ['nama'] // Get nama from Pemasok
      })
        // Make update query
        var update = {
          nama: req.body.nama,
          harga: req.body.harga,
          id_pemasok: req.body.id_pemasok,
          image:req.file===undefined?"":req.file.filename
        }
  
        // Barang table update data
        return Barang.update(update, {
          where: {
            id: req.params.id
          }
      }).then(affectedRow => {
        return Barang.findOne({ // find one data of Barang table
          where: {
            id: req.params.id // where id of Barang table is equal to req.params.id
          }
        })
      }).then(b => {
        // Send response JSON and get one of Barang table that we've updated
        res.json({
          "status": "success",
          "message": "barang updated",
          "data": b
        })
      })
    }
    // Soft delete Barang data
    async delete(req, res) {
      Barang.destroy({ // Delete data from Barang table
        where: {
          id: req.params.id // Where id of Barang table is equal to req.params.id
        }
      }).then(affectedRow => {
        // If delete success, it will return this JSON
        if (affectedRow) {
          return {
            "status": "success",
            "message": "barang deleted",
            "data": null
          }
        }
  
        // If failed, it will return this JSON
        return {
          "status": "error",
          "message": "Failed",
          "data": null
        }
      }).then(r => {
        res.json(r) // Send response JSON depends on failed or success
      })
    }
  
  }
  
  module.exports = new BarangController;
  