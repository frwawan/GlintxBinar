const {Pemasok} = require("../models")
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

class PemasokController {

  // Get All data from Pemasok
  async getAll(req, res) {
    Pemasok.findAll({ // find all data of Pemasok table
      attributes: ['id', 'nama', ['createdAt', 'waktu']] // just these attributes that showed
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get all of Pemasok table
    })
  }
  // Get One data from Pemasok
  async getOne(req, res) {
    Pemasok.findOne({ // find one data of Pemasok table
      where: {
        id: req.params.id // where id of Pemasok table is equal to req.params.id
      },
      attributes: ['id', 'nama', ['createdAt', 'waktu']] // just these attributes that showed
    }).then(pemasok => {
      res.json(pemasok) // Send response JSON and get one of Pemasok table depend on req.params.id
    })
  }
  // Create Pemasok data
  async create(req, res) {
      // Pemasok table create data
      Pemasok.create({
        nama : req.body.nama,
    }).then(newpemasok => {
      // Send response JSON and get one of Pemasok table that we've created
      res.json({
        "status": "success",
        "message": "pemasok added",
        "data": newpemasok
      })
    })
  }
  // Update Pemasok data
  async update(req, res) {
      // Make update query
      var update = {
        nama: req.body.nama,
      }

      // Pemasok table update data
      Pemasok.update(update, {
        where: {
          id: req.params.id
        }
    }).then(affectedRow => {
      return Pemasok.findOne({ // find one data of Pemasok table
        where: {
          id: req.params.id // where id of Pemasok table is equal to req.params.id
        }
      })
    }).then(b => {
      // Send response JSON and get one of Pemasok table that we've updated
      res.json({
        "status": "success",
        "message": "pemasok updated",
        "data": b
      })
    })
  }
  // Soft delete Pemasok data
  async delete(req, res) {
    Pemasok.destroy({ // Delete data from Pemasok table
      where: {
        id: req.params.id // Where id of Pemasok table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "pemasok deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }

}

module.exports = new PemasokController;
