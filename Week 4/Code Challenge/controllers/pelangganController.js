const {Pelanggan} = require("../models")
const {
  check,
  validationResult,
  matchedData,
  sanitize
} = require('express-validator'); //form validation & sanitize form params

class PelangganController {

  // Get All data from Pelanggan
  async getAll(req, res) {
    Pelanggan.findAll({ // find all data of Pelanggan table
      attributes: ['id', 'nama', ['createdAt', 'waktu']] // just these attributes that showed
    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get all of Pelanggan table
    })
  }
  // Get One data from Pelanggan
  async getOne(req, res) {
    Pelanggan.findOne({ // find one data of Pelanggan table
      where: {
        id: req.params.id // where id of Pelanggan table is equal to req.params.id
      },
      attributes: ['id', 'nama', ['createdAt', 'waktu']] // just these attributes that showed
    }).then(pelanggan => {
      res.json(pelanggan) // Send response JSON and get one of Pelanggan table depend on req.params.id
    })
  }
  // Create Pelanggan data
  async create(req, res) {
      // Pelanggan table create data
      Pelanggan.create({
        nama : req.body.nama,
    }).then(newpelanggan => {
      // Send response JSON and get one of Pelanggan table that we've created
      res.json({
        "status": "success",
        "message": "pelanggan added",
        "data": newpelanggan
      })
    })
  }
  // Update Pelanggan data
  async update(req, res) {
      // Make update query
      var update = {
        nama: req.body.nama,
      }

      // Pelanggan table update data
      Pelanggan.update(update, {
        where: {
          id: req.params.id
        }
    }).then(affectedRow => {
      return Pelanggan.findOne({ // find one data of Pelanggan table
        where: {
          id: req.params.id // where id of Pelanggan table is equal to req.params.id
        }
      })
    }).then(b => {
      // Send response JSON and get one of Pelanggan table that we've updated
      res.json({
        "status": "success",
        "message": "pelanggan updated",
        "data": b
      })
    })
  }
  // Soft delete Pelanggan data
  async delete(req, res) {
    Pelanggan.destroy({ // Delete data from Pelanggan table
      where: {
        id: req.params.id // Where id of Pelanggan table is equal to req.params.id
      }
    }).then(affectedRow => {
      // If delete success, it will return this JSON
      if (affectedRow) {
        return {
          "status": "success",
          "message": "pelanggan deleted",
          "data": null
        }
      }

      // If failed, it will return this JSON
      return {
        "status": "error",
        "message": "Failed",
        "data": null
      }
    }).then(r => {
      res.json(r) // Send response JSON depends on failed or success
    })
  }

}

module.exports = new PelangganController;
